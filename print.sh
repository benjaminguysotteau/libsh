#!/bin/bash
################################################################################
#
# Filename: install.sh
#   Author: Benjamin GUY SOTTEAU
#     Mail: benjamin.guysotteau@heurtaux.fr
#  Created: 2018-06-25
#
################################################################################
#set -x

# SOURCE
# ------
# https://stackoverflow.com/questions/5947742/how-to-change-the-output-color-of-echo-in-linux
# https://misc.flogisoft.com/bash/tip_colors_and_formatting
# https://www.admin-linux.fr/bash-de-la-couleur-dans-le-shell/

# Num  Colour    #define         R G B
# 
# 0    black     COLOR_BLACK     0,0,0
# 1    red       COLOR_RED       1,0,0
# 2    green     COLOR_GREEN     0,1,0
# 3    yellow    COLOR_YELLOW    1,1,0
# 4    blue      COLOR_BLUE      0,0,1
# 5    magenta   COLOR_MAGENTA   1,0,1
# 6    cyan      COLOR_CYAN      0,1,1
# 7    white     COLOR_WHITE     1,1,1

# On affiche le premier crochet avec les espaces à la suite,
# c'est le même pour tous sauf pour le message ok qui à un espace de plus,
# celon la valeur du premier argument on affiche la couleur,
# ainsi que le marqueur entre crochet,
# Si la valeur du premier argument n'est pas bonne on sort.
# On affiche le second crochet en blanc avec le nombre d'espaces en fonction de l'argument.
# Si le second argument n'est pas bon on sort,
# on affiche le message final.
print_formatted()
{
  prefix_text=$1
  text_to_print=$2

  if [[ "$1" = "ok" ]]; then
    echo -n '[  '
  else
    echo -n '[ '
  fi

  if   [[ "$1" = "ok" ]]; then
    tput setaf 2
    echo -n 'OK'

  elif [[ "$1" = "warn" ]]; then
    tput setaf 3
    echo -n 'WARN'

  elif [[ "$1" = "error" ]]; then
    tput setaf 1
    echo -n 'ERROR'

  elif [[ "$1" = "fail" ]]; then
    tput setaf 5
    echo -n 'FAIL'

  elif [[ "$1" = "info" ]]; then
    tput setaf 4
    echo -n 'INFO'

  else
    echo "$0, ERROR: print_formatted, first argument unrecognized!"
    exit 1
  fi
  
  tput setaf 7

  if   [[ "$1" = "ok" ]]; then
    echo -n '  ] '

  elif [[ "$1" = "warn" ]] \
    || [[ "$1" = "fail" ]] \
    || [[ "$1" = "info" ]]; then
    echo -n ' ] '

  elif [[ "$1" = "error" ]]; then
    echo -n '] '

  fi

  if [[ -z "$2" ]]; then
    echo "$0, ERROR: print_formatted, second argument is empty!"
    exit 2
  fi

  echo -n $text_to_print
  echo ''
}

#set +x
