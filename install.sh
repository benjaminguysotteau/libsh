#!/bin/bash
################################################################################
#
# Filename: install.sh
#   Author: Benjamin GUY SOTTEAU
#     Mail: benjamin.guysotteau@heurtaux.fr
#  Created: 2018-06-25
#
################################################################################
#set -x

source ./print.sh

is_install()
{
  package=$1

  # On cherche dans le path et
  # dans les gestionnaire de paquets debian et python.
  if [[ ! -z $(which $package) ]] \
  || [[ ! -z $(dpkg --list | grep -i $package | awk '{print $2}') ]] \
  || [[ ! -z $(pip list | grep -i $package | awk '{print $1}') ]]; then
    # Pour retourner la valeur, il faut faire un echo,
    # puis un return sinon en sortant du if le "echo 0"
    # sera lu lui aussi.
    echo 1 && return
  fi
  echo 0
}

is_not_install()
{
  package=$1

  if [[ -z $(which $package) ]] \
  && [[ -z $(dpkg --list | grep -i $package | awk '{print $2}') ]] \
  && [[ -z $(pip list | grep -i $package | awk '{print $1}') ]]; then
    echo 1 && return
  fi
  echo 0
}

install_recommends()
{
  package=$1

  print_formatted info "Sudo password required to installation package(s): $package"
  sudo apt-get install --install-recommends $package
}

#set +x
